//! Basic structures and types.

use ansi::{ Style, Colour } ;

use grid::Status ;

static colors: [Colour ; 6] = {
  use ansi::Colour::* ;
  [ Red, Green, Yellow, Blue, Purple, Cyan ]
};

/// Prints an error and exit with status `2`.
#[allow(dead_code)]
pub fn error(err: String) -> ! {
  use std::process::exit ;

  let err_style = Colour::Red.bold() ;
  println!("") ;
  let mut lines = err.lines() ;

  // Print error header for first line.
  print!("{} Error, ", err_style.paint("|===|")) ;

  'find_first_non_empty: loop {
    match lines.next() {
      Some(line) if line != "" => {
        // First line (follows error header).
        println!("{}", line) ;
        // Prefix other lines with an indicator.
        for line in lines {
          println!("{} {}", err_style.paint("|"), line)
        } ;
        break 'find_first_non_empty
      },
      // No explanation, or empty one.
      _ => println!("no explanation available..."),
    }
  }
  print!("{}", err_style.paint("|===|")) ;
  println!("") ;
  exit(2)
}

/// Returns a color for an ID.
fn color_of(n: usize) -> Colour {
  colors[n]
}

/// Game of life configuration.
pub struct Conf {
  /// Height of the grid.
  hgt: usize,
  /// Width of the grid.
  wdt: usize,
  /// Vector of styles for rendering.
  styles: Vec<Style>,
  /// Player names.
  names: Vec<(String, Style)>,
}
impl Conf {
  /// Creates a configuration.
  #[inline]
  pub fn mk(hgt: usize, wdt: usize, names: Vec<String>) -> Self {
    use ansi::Colour::* ;
    let len = names.len() ;
    let (_, names) = names.into_iter().fold(
      (0, Vec::with_capacity(len)),
      |(cnt, mut vec), name| {
        vec.push( (name, color_of(cnt).normal()) ) ;
        (cnt + 1, vec)
      }
    ) ;
    Conf {
      hgt: hgt, wdt: wdt,
      styles: vec![
        Style::new().on( Red ),
        Style::new().on( Green ),
        Style::new().on( Yellow ),
        Style::new().on( Blue ),
        Style::new().on( Purple ),
        Style::new().on( Cyan ),
      ],
      names: names,
    }
  }
  /// Height of the grid.
  #[inline]
  pub fn hgt(& self) -> usize { self.hgt }
  /// Width of the grid.
  #[inline]
  pub fn wdt(& self) -> usize { self.wdt }

  /// Player names.
  #[inline]
  pub fn players(& self) -> & [ (String, Style) ] {
    & self.names
  }

  /// Clamps some coordinates to the grid size.
  #[inline]
  pub fn clamp(& self, row: i32, col: i32) -> (usize, usize) {
    let row = if row < 0 {
      self.hgt - (row.abs() as usize)
    } else {
      (row as usize) % self.hgt
    } ;
    let col = if col < 0 {
      self.wdt - (col.abs() as usize)
    } else {
      (col as usize) % self.wdt
    } ;
    (row, col)
  }

  /// Paints and prints a cell.
  pub fn praint(& self, cell: & Status) {
    use grid::Status::* ;
    match * cell {
      Dead => print!("  "),
      Live(n) => print!(
        "{}", self.styles[
          n % self.styles.len()
        ].paint("  ")
      ),
    }
  }
}

