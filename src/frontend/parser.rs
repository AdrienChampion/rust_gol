/*! Parser and AST to `SubGrid` functions. */

use cfg::{ Syntax, MetaData } ;
use range::Range ;

use grid::Status ;
use frontend::SubGrid ;

/// Creates the parser for user initial sub-grids.
pub fn mk_parser() -> Result<Syntax, String> {
  use cfg::* ;
  let rules = r#"
    /* Single line comments using `//`. */
    0 sl_comment = {
      [.w? "//" ..."\n"?]
      [.w? "//" ..."\r"?]
    }

    /* Some comments and / or whitespaces. */
    11 cws = { .s!(.w? sl_comment) .w? }
    1 cmt_or_ws = cws

    /* Cells. */
    21 dead = { "0":!"alive" "_":!"alive" ".":!"alive" }
    22 alive = { "1":"alive" "*":"alive" "x":"alive" }
    2 cell = { dead alive }

    /* Sub-grid. Rows are comma-separated, cells are whitespace-separated. */
    31 row = .s!(cmt_or_ws cell)
    32 rows = [
      cmt_or_ws .s!([cmt_or_ws "," cmt_or_ws] row:"row") cmt_or_ws
    ]
    3 sub_grid = [
      "grid" cmt_or_ws "("
        cmt_or_ws .$_:"n_rows" cmt_or_ws "," cmt_or_ws .$_:"n_cols" cmt_or_ws
      ")" cmt_or_ws "{" rows "}"
    ]

    /* Player info. */
    4 player_info = ["player" cmt_or_ws ":" cmt_or_ws .t!:"player"]

    /* Entry point of the parser. */
    5 entry_point = [
      cmt_or_ws player_info
      cmt_or_ws sub_grid
      cmt_or_ws
    ]
  "# ;
  
  match syntax_errstr(rules) {
    Err(err) => return Err(
      format!("could not create parser:\n{}", err)
    ),
    Ok(parser) => Ok(parser),
  }
}


/** Unwraps an option of a result.

Returns `something` if the input is `Some(Ok(something))`, exits the current
function with an error otherwise.

The second parameter is the token expected, used to document the error if there
is no more tokens. */
macro_rules! try_next_token {
  ($to_try:expr, expecting $s:expr) => (
    match $to_try {
      Some( Ok(something) ) => something,
      Some( Err(e) ) => return Err(e),
      None => return Err(
        format!("expected {}, got nothing", $s)
      ),
    } ;
  ) ;
}

/** Applies a function to the next token in a token iterator, if any. */
fn next_token_apply<
  'a,
  TknIter: Iterator<Item = & 'a Range<MetaData>>,
  Output,
  Fun: Fn(& 'a Range<MetaData>) -> Output,
>(
  tokens: & mut TknIter, f: Fun
) -> Option<Output> {
  match tokens.next() {
    Some(tkn) => Some( f(tkn) ),
    None => None,
  }
}

/// Turns an AST produced by parsing using the parser from `mk_parser` into a
/// `SubGrid`.
pub fn ast_to_sub_grid(
  id: usize, ast: & [ Range<MetaData> ]
) -> Result<SubGrid, String> {
  use cfg::MetaData::* ;

  let mut tokens = ast.iter() ;

  // First token should be the player.
  let player = try_next_token!(
    next_token_apply(
      & mut tokens,
      |tkn| match tkn.data {
        String(ref key, ref val) => if ** key == "player" {
          Ok((** val).clone())
        } else {
          Err(
            format!(
              "expected player\ngot key: {}\n    value: {}", key, val
            )
          )
        },
        _ => Err(
          format!("expected player\ngot {:?}", tkn)
        ),
      }
    ), expecting "player"
  ) ;

  // Second token should be the number of rows.
  let rows = try_next_token!(
    next_token_apply(
      & mut tokens,
      |tkn| match tkn.data {
        F64(ref key, ref val) => if ** key == "n_rows" {
          Ok(* val as usize)
        } else {
          Err(
            format!(
              "expected rows count\ngot key: {}\n    value: {}", key, val
            )
          )
        },
        _ => Err(
          format!("expected rows count\ngot {:?}", tkn)
        ),
      }
    ), expecting "n_rows"
  ) ;

  // Second token should be the number of cols.
  let cols = try_next_token!(
    next_token_apply(
      & mut tokens,
      |tkn| match tkn.data {
        F64(ref key, ref val) => if ** key == "n_cols" {
          Ok(* val as usize)
        } else {
          Err(
            format!(
              "expected cols count\ngot key: {}\n    value: {}", key, val
            )
          )
        },
        _ => Err(
          format!("expected cols count\ngot {:?}", tkn)
        ),
      }
    ), expecting "n_cols"
  ) ;

  // Preparing an empty list of rows with the right capacity.
  let mut grid = Vec::with_capacity(rows) ;
  // Preparing an empty list of cells with the right capacity.
  let mut row = Vec::with_capacity(cols) ;

  // Tokens left should only be the grid.
  for token in tokens {
    match token.data {
      // End of `row` node.
      EndNode(ref id) if ** id == "row" => {
        // Push current row to grid.
        grid.push(row) ;
        // Create new row.
        row = Vec::with_capacity(cols)
      },

      // Start of `row` node.
      StartNode(ref id) if ** id == "row" => (),

      // Cell data.
      Bool(ref key, ref val) => if ** key == "alive" {
        // Pushing new cell to current row.
        row.push(
          if * val { Status::Live(id) } else { Status::Dead }
        )
      } else {
        return Err(
          format!("expected cell\ngot{:?}", token)
        )
      },

      _ => panic!("aaahhh: {:?}", token),
    }
  } ;

  // Check for dimension mismatch (rows).
  if grid.len() != rows {
    return Err(
      format!(
        "expected sub-grid with {} rows\ngot {} rows", rows, grid.len()
      )
    )
  } ;
  // Check for dimension mismatch (columns).
  for i in 0..grid.len() {
    if grid[i].len() != cols {
      return Err(
        format!(
          "expected sub-grid with {} columns\nbut row {} has {} columns",
          cols, i, grid[i].len()
        )
      )
    }
  } ;

  // Add sub-grid to internal list of subgrids.
  Ok( SubGrid::mk(player, grid) )
}


