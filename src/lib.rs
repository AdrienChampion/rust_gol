//! Conway's game of life. Rust implementation, as a library.

#![allow(non_upper_case_globals)]

extern crate ansi_term as ansi ;
extern crate ansi_control ;
extern crate piston_meta as cfg ;
extern crate range ;

mod common ;
pub use common::Conf ;
mod grid ;
pub use grid::* ;
pub mod frontend ;