//! Moves the cursor in the terminal.

use grid::Grid ;
use ansi_control::* ;

pub fn goto_top<G: Grid>(grid: & G) {
  print!("{}{}", set_column(0), clear_line(Pos::Both)) ;
  for _ in 0..(grid.size().0 + 5) {
    print!("{}{}", move_cursor(1, 0), clear_line(Pos::Both))
  }
}

pub fn clear() {
  print!("{}", clear_line(Pos::Both))
}

pub fn goto_question_line() {
  print!(
    "{}{}{}",
    set_column(0), move_cursor(1,0), clear_line(Pos::Both)
  ) ;
}