//! Contains the grid structures and operations.

use std::fmt ;

use common::* ;

/// Status of a cell.
#[ derive(Clone, Debug, PartialEq, Eq) ]
pub enum Status {
  /// Cell's dead.
  Dead,
  /// Cell's alive, integer indicates player ID.
  Live(usize),
}
impl fmt::Display for Status {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    match * self {
      Status::Dead => write!(fmt, "-"),
      Status::Live(n) => write!(fmt, "{}", n),
    }
  }
}


/// Abstraction of a grid, can access and modify elements.
pub trait Grid {
  /// Score.
  #[inline]
  fn score(& self) -> & [usize] ;
  /// Underlying configuration.
  fn conf(& self) -> & Conf ;
  #[inline]
  /// The size of the grid (height, width).
  fn size(& self) -> (usize, usize) ;
  #[inline]
  /// Status of a cell in a grid.
  fn get(& self, usize, usize) -> & Status ;
  #[inline]
  /// Status of a cell in a grid, mutable.
  fn get_mut(& mut self, usize, usize) -> & mut Status ;
  #[inline]
  /// Gets the value of a cell.
  fn old_get(& self, row: usize, col: usize) -> & Status ;
  /// The (current) grid.
  fn grid(& self) -> & [ Vec<Status> ] ;
}

/// Internal operations on a grid.
pub trait GridOps: Grid {
  /// Swaps the current grid and the old one.
  fn swap(& mut self) ;
  /// The new value of a cell based on the old one.
  fn next(& mut self, usize, usize) -> Result<Status, String> ;
  /// The new grid.
  fn nu(& mut self) -> Option< Vec< Vec<Status> > > ;
  /// Sets the new grid.
  fn set_nu(& mut self, Vec< Vec<Status> >) -> Result<(), String> ;

  /// Update function, sequential.
  fn update_seq(& mut self) -> Result<bool, String> {
    let (hgt, wdt) = (self.conf().hgt(), self.conf().wdt()) ;
    self.swap() ;
    let mut nu = match self.nu() {
      Some(nu) => nu,
      None => return Err("could not retrieve new grid".to_string()),
    } ;
    let mut fixed_point = true ;
    for row in 0..hgt {
      for col in 0..wdt {
        match self.next(row, col) {
          Ok(status) => {
            fixed_point = fixed_point && status == * self.old_get(row, col) ;
            if fixed_point && status != * self.old_get(row, col) {
              println!("nu: {}, old: {}", status, self.old_get(row, col))
            } ;
            nu[row][col] = status ;
          },
          Err(e) => return Err(e),
        }
      }
    } ;
    match self.set_nu(nu) {
      Ok(_) => (),
      Err(e) => return Err(e),
    } ;
    let one_or_zero_left = self.score().iter().fold(
      0, |cnt, n| if * n == 0 { cnt } else { cnt + 1 }
    ) <= 1 ;
    Ok(fixed_point || one_or_zero_left)
  }
}



/// Real implementation of a grid.
pub struct RealGrid {
  /// Old grid.
  old: Vec< Vec<Status> >,
  /// New, current grid.
  new: Option< Vec< Vec<Status> > >,
  /// Grid configuration.
  conf: Conf,
  /// Score.
  score: Vec<usize>,
}
impl RealGrid {
  /// Creates a new, empty grid given a configuration.
  pub fn empty(conf: Conf) -> Self {
    let empty = vec![ vec![ Status::Dead ; conf.wdt() ] ; conf.hgt() ] ;
    let len = conf.players().len() ;
    RealGrid {
      old: empty.clone(),
      new: Some(empty),
      conf: conf,
      score: vec![0 ; len]
    }
  }
  /// Creates a new grid given a configuration.
  pub fn mk(grid: Vec< Vec<Status> >, conf: Conf) -> Self {
    let mut score = vec![0 ; conf.players().len()] ;
    for row in grid.iter() {
      for cell in row.iter() {
        match * cell {
          Status::Dead => (),
          Status::Live(ref n) => score[* n] += 1,
        }
      }
    } ;
    RealGrid {
      old: grid.clone(),
      new: Some(grid),
      conf: conf,
      score: score,
    }
  }
}
impl Grid for RealGrid {
  fn score(& self) -> & [usize] {
    & self.score
  }
  fn conf(& self) -> & Conf { & self.conf }
  fn size(& self) -> (usize, usize) { (self.conf.wdt(), self.conf.hgt()) }
  fn get(& self, r: usize, c: usize) -> & Status {
    & self.new.as_ref().unwrap()[r][c]
  }
  fn get_mut(& mut self, r: usize, c: usize) -> & mut Status {
    match self.new.as_mut().unwrap().get_mut(r) {
      Some(line) => match line.get_mut(c) {
        Some(status) => status,
        None => unreachable!(),
      },
      None => unreachable!(),
    }
  }
  fn old_get(& self, row: usize, col: usize) -> & Status {
    & self.old[row][col]
  }
  fn grid(& self) -> & [ Vec<Status> ] { & self.new.as_ref().unwrap() }
}

impl GridOps for RealGrid {
  fn swap(& mut self) {
    match self.new.as_mut() {
      Some(mut new) => {
        ::std::mem::swap(& mut self.old, & mut new)
      },
      None => panic!("aaa"),
    }
  }
  fn next(& mut self, r: usize, c: usize) -> Result<Status, String> {
    use grid::Status::* ;
    use std::collections::HashMap ;
    use std::cmp::Ordering::* ;
    match self.old[r][c] {
      Dead => {
        let mut map = HashMap::with_capacity(self.conf().players().len()) ;
        for d_r in -1i32..2i32 {
          for d_c in -1i32..2i32 {
            if d_r != 0 || d_c != 0 {
              let (r, c) = self.conf.clamp(
                (r as i32) + d_r, (c as i32) + d_c
              ) ;
              match self.old[r][c] {
                Live(n) => {
                  match map.get_mut(& n) {
                    Some(cnt) => { * cnt += 1 ; continue },
                    _ => (),
                  } ;
                  match map.insert(n, 1) {
                    None => (),
                    Some(_) => unreachable!(),
                  }
                },
                _ => (),
              }
            }
          }
        } ;
        if map.is_empty() { Ok(Dead) } else {
          if map.len() == 1 {
            for (id, cnt) in map {
              if cnt == 3 {
                self.score[id] += 1 ;
                return Ok(Live(id))
              } else {
                return Ok(Dead)
              }
            } ;
            unreachable!()
          } else {
            let mut res = (0, Dead) ;
            for (id, cnt) in map {
              if cnt >= 3 {
                match cnt.cmp(& res.0) {
                  Greater => res = (cnt, Live(id)),
                  Equal => match res.1 {
                    Live(_) => res.1 = Dead,
                    Dead => (),
                  },
                  Less => (),
                }
              }
            } ;
            match res.1 {
              Live(n) => {
                self.score[n] += 1 ;
                Ok( Live(n) )
              },
              Dead => Ok(Dead),
            }
          }
        }
      },
      Live(n) => {
        let mut cnt = 0 ;
        for d_r in -1i32..2i32 {
          for d_c in -1i32..2i32 {
            if d_r != 0 || d_c != 0 {
              let (r, c) = self.conf.clamp(
                (r as i32) + d_r, (c as i32) + d_c
              ) ;
              match self.old[r][c] {
                Live(m) if n == m => cnt += 1,
                Live(_) => cnt = if cnt == 0 { cnt } else { cnt - 1 },
                _ => (),
              }
            }
          }
        } ;
        let status = if 2 <= cnt && cnt <= 3 {
          Live(n)
        } else {
          self.score[n] -= 1 ;
          Dead
        } ;
        Ok(status)
      },
    }
  }
  fn nu(& mut self) -> Option< Vec< Vec<Status> > > {
    let mut nu = None ;
    ::std::mem::swap(& mut self.new, & mut nu) ;
    nu
  }
  fn set_nu(& mut self, grid: Vec< Vec<Status> >) -> Result<(), String> {
    if self.new.is_some() {
      Err("trying to overwrite current grid".to_string())
    } else {
      self.new = Some(grid) ;
      Ok(())
    }
  }
}
